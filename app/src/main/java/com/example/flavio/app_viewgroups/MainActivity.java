package com.example.flavio.app_viewgroups;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void const_click(View view) {
          Intent i = new Intent(this, ConstraintIMC.class);
          startActivity(i);
    }

    public void abs_click(View view) {
        Intent i = new Intent(this, AbsoluteCelcius.class);
        startActivity(i);
    }

    public void linear_click(View view) {
        Intent i = new Intent(this, LinearSorteio.class);
        startActivity(i);
    }



}
