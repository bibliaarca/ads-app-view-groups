package com.example.flavio.app_viewgroups;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class LinearSorteio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear_sorteio);

    }

    public static double getRandomDoubleBetweenRange(double min, double max){

        double x = (Math.random()*((max-min)+1))+min;

        return x;

    }

    public void gerar_random(View view) {
        EditText edtNumero1 = (EditText) findViewById(R.id.edNumero1);
        EditText edtNumero2 = (EditText) findViewById(R.id.edNumero2);
        TextView edtNumeroEscolhido =  (TextView) findViewById(R.id.EdNumeroEscolhido);
        double n1 = Double.parseDouble(edtNumero1.getText().toString());
        double n2 = Double.parseDouble(edtNumero2.getText().toString());

        double r = getRandomDoubleBetweenRange(n1, n2);
        edtNumeroEscolhido.setText(  String.format("%.2f", r) );
    }
}
