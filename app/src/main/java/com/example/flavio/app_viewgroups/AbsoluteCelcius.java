package com.example.flavio.app_viewgroups;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AbsoluteCelcius extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absolute_celcius);
    }

    public void converterCelcuisFarenheit(View view) {


                EditText edtCelcius = (EditText) findViewById(R.id.edtCelcius);
                EditText edtFarenheit = (EditText) findViewById(R.id.edtFarenheit);

                double v_celcius   = Double.parseDouble(edtCelcius.getText().toString());
                double v_farenheit = v_celcius * 1.8 + 32;

                edtFarenheit.setText(  String.format("%.2f", v_farenheit) );


    }
}
